if (Meteor.isClient) {

  Template.tickets.helpers({
    tickets: function () {
      return Tickets.find();
    }
  });

  Template.newTickets.events({
    'click #submit': function (e, t) {
      e.preventDefault();
      var ticket = t.find('#ticket').value;
      Tickets.insert({name:ticket});
    }

  });

  Template.addFile.events({
    'click #addFile' : function(e, t){
      var file = t.find('#file').files[0];
      var ticket = this._id;
      var newFile = new FS.File(file);
      newFile.metadata = {
        ticketId: ticket
      };
      Files.insert(newFile, function (err, fileObj) {
        if (!err) {
          console.log(fileObj);
        }
      });
    }
  });

  Template.addFile.helpers({
    files: function() {
      var ticket = this._id;
      return Files.find({'metadata.ticketId':ticket});
    }
  })
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    console.log('server works');
  });
}
